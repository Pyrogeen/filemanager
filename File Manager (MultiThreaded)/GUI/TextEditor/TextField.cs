using System.Collections.Generic;

namespace File_Manager__MultiThreaded_.GUI.TextEditor
{
    public class TextField : Text
    {
        public TextField(string text)
        {
            Items = new List<char>();
            Items.AddRange(text);
            Items.Add(' ');
            Selected = Items[0];
        }
    }
}