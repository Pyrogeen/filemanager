﻿using System;
using System.Collections.Generic;
using File_Manager__MultiThreaded_.GUI.Windows;

namespace File_Manager__MultiThreaded_.GUI.TextEditor
{
    public abstract class Text
    {
        public List<char> Items { get; protected set; }
        public int Selected { get; protected set; }
        public int ScrollOffset { get; protected set; }

        public int X { get; private set; }
        public int Y { get; private set; }
        public int Width { get; private set; }

        public virtual void SetProperties(int x, int y, int width)
        {
            X = x;
            Y = y + 1;
            Width = width;
            Console.CursorVisible = true;
        }

        public virtual void SetProperties(Window window)
        {
            X = window.X;
            Y = window.Y + 1;
            Width = window.Width;
            Console.CursorVisible = true;
        }

        public virtual bool HandleKey(ConsoleKeyInfo key, WindowManager browserWindows)
        {
            switch (key.Key)
            {
                // Change selected
                case ConsoleKey.LeftArrow:
                    if (ChangeSelected(-1))
                        Renderer.RefreshText(this);
                    break;

                // Change selected
                case ConsoleKey.RightArrow:
                    if (ChangeSelected(1))
                        Renderer.RefreshText(this);
                    break;

                // Delete
                case ConsoleKey.Backspace:
                    if (Selected - 1 < 0) break;
                    ChangeSelected(-1, -1);
                    Items.RemoveAt(Selected);
                    Renderer.RefreshText(this);
                    break;

                // Delete
                case ConsoleKey.Delete:
                    if (Selected > Items.Count - 2) break;
                    Items.RemoveAt(Selected);
                    ChangeSelected(0, -1);
                    Renderer.RefreshText(this);
                    break;

                // To first char
                case ConsoleKey.Home:
                    ScrollOffset = 0;
                    Selected = 0;
                    Renderer.RefreshText(this);
                    break;

                // To last char
                case ConsoleKey.End:
                    ScrollOffset = Items.Count > Width ? Items.Count - Width + 1 : 0;
                    Selected = Items.Count - 1;
                    Renderer.RefreshText(this);
                    break;

                // RefreshWindow
                case ConsoleKey.F5:
                    browserWindows.SetWindows();
                    Renderer.DrawAll(browserWindows);
                    break;

                default:
                    if (char.IsControl(key.KeyChar)) break;
                    Items.Insert(Selected, key.KeyChar);
                    ChangeSelected(1);
                    Renderer.RefreshText(this);
                    break;
            }

            return false;
        }

        protected virtual bool ChangeSelected(int offset, int forced = 0)
        {
            int offsetIndex = Selected + offset;
            if (forced != 0 && ScrollOffset > 0)
            {
                ScrollOffset += forced;
                Selected = offsetIndex;
                return true;
            }

            if (offsetIndex < 0 || offsetIndex > Items.Count - 1) return false;

            if (offsetIndex >= ScrollOffset && offset < 0 ||
                offsetIndex - ScrollOffset < Width - 2 && offset > 0)
            {
                Selected = offsetIndex;
                return true;
            }

            ScrollOffset += offset;
            Selected = offsetIndex;
            return true;
        }
    }
}