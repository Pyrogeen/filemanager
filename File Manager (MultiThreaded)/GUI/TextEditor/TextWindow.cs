﻿using System;
using System.IO;
using File_Manager__MultiThreaded_.GUI.Windows;
using File_Manager__MultiThreaded_.Utilities;

namespace File_Manager__MultiThreaded_.GUI.TextEditor
{
    public class TextWindow : Window
    {
        public TextWindow(FileInfo file)
        {
            File = file;
            Items = new Items<object>();
            Operations.GetFile(file, this);
            SetProperties();
        }

        public FileInfo File { get; }


        public void SetProperties()
        {
            SetProperties(0, 1, Console.WindowWidth + 1, Console.WindowHeight - 2);

            int selected = Items?.Selected == null ? 0 : Items.IndexOf(Items.Selected);

            ScrollOffset = 0;

            if (Items != null)
            {
                Items.Selected = Items[selected <= Items.Count - 1 && selected - ScrollOffset < Height ? selected : 0];
                Items.LastSelected = Items.Selected;
                
                foreach (TextField textField in Items)
                {
                    textField.SetProperties(0, Items.IndexOf(textField) + 1, Width);
                }
            }

            Console.CursorVisible = true;
        }


        public override bool HandleKey(ConsoleKeyInfo key, WindowManager browserWindows)
        {
            if (((Text) Items.Selected).HandleKey(key, browserWindows))
                return true;

            switch (key.Key)
            {
                // Change selected
                case ConsoleKey.UpArrow:
                    Items.LastSelected = Items.Selected;
                    if (ChangeSelected(-1))
                        Renderer.Scroll(this, -1, true);
                    Renderer.Highlight(this);
                    break;

                // Change selected
                case ConsoleKey.DownArrow:
                    Items.LastSelected = Items.Selected;
                    if (ChangeSelected(1))
                        Renderer.Scroll(this, 1, true);
                    Renderer.Highlight(this);
                    break;
                
                case ConsoleKey.Escape:
                    Console.CursorVisible = false;
                    return true;
            }

            return false;
        }
    }
}