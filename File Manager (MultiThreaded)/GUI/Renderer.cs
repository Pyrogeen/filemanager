﻿using File_Manager__MultiThreaded_.GUI.Dialogs;
using File_Manager__MultiThreaded_.GUI.TextEditor;
using File_Manager__MultiThreaded_.GUI.Windows;
using File_Manager__MultiThreaded_.Utilities;
using System;
using System.Drawing;
using System.IO;
using Console = Colorful.Console;

namespace File_Manager__MultiThreaded_.GUI
{
    public static class Renderer
    {
        public static void DrawAll(WindowManager browserWindows)
        {
            SetDefaultColors();
            try
            {
                Console.Clear();
            }
            catch (IOException)
            {
                /* TODO */
            }

            if (WindowManager.MainWindow != null)
            {
                DrawWindow(WindowManager.MainWindow, true);
                Highlight(WindowManager.MainWindow);
                return;
            }

            foreach (BrowserWindow browser in browserWindows.Windows)
                switch (browser.SubWindow)
                {
                    case null:
                    {
                        DrawWindow(browser, true);
                        if (browserWindows.SelectedWindow == browser)
                            Highlight(browser);
                        else
                            DeactivateHighlight(browser);
                        break;
                    }

                    case PropertiesWindow _:
                    {
                        DrawWindow(browser.SubWindow, true);
                        if (browserWindows.SelectedWindow.SubWindow == browser.SubWindow)
                            Highlight(browser.SubWindow);
                        else
                            DeactivateHighlight(browser.SubWindow);
                        break;
                    }

                    case DialogWindow _:
                        DrawWindow(browser, true);
                        DeactivateHighlight(browser);
                        DrawWindow(browser.SubWindow, true);
                        if (browserWindows.SelectedWindow == browser)
                        {
                            if (browser.SubWindow is TextDialog textDialog && textDialog.TextBox != null)
                                RefreshText(textDialog.TextBox);
                            else
                                Highlight(browser.SubWindow);
                        }
                        else
                        {
                            DeactivateHighlight(browser.SubWindow);
                        }

                        break;
                }

            DrawButtons(new[]
            {
                "1|Info", "2|RenMove", "3|New", "4|Del", "5|Refresh", "6|Copy", "7|CopyTo", "8|Paste", "+|Add", "-|Rem",
                "Esc|Quit"
            });

            SetDefaultColors();
        }

        public static void RefreshWindow(Window window, bool clearWindow)
        {
            DrawWindow(window, clearWindow);
            if (window is TextDialog textDialog && textDialog.TextBox != null)
                RefreshText(textDialog.TextBox);
            else
                Highlight(window);
        }

        public static void RefreshText(Text textBox)
        {
            int itemsCount;

            if (textBox.Width - 2 > textBox.Items.Count)
                itemsCount = textBox.Items.Count;
            else
                itemsCount = textBox.Width - 2;

            WriteText(ColorScheme.TextWindowForeground, ColorScheme.TextWindowBackground, textBox.X,
                textBox.Y, textBox.Items.GetRange(textBox.ScrollOffset, itemsCount).GetString().Pad
                    (textBox.Width));

            Console.SetCursorPosition(textBox.X + textBox.Selected - textBox.ScrollOffset, textBox.Y);
        }

        private static void DrawWindow(Window window, bool clearWindow)
        {
            if (clearWindow)
                Clear(window);

            switch (window)
            {
                case DialogWindow _:
                    DrawHeader(window);

                    Console.BackgroundColor = ColorScheme.DialogWindowBackground;
                    for (int index = 0;
                        index < window.Height - 1 && index < window.Items.Count;
                        index++)
                    {
                        if (Console.ForegroundColor != window.Items[index + window.ScrollOffset].GetForegroundColor())
                            Console.ForegroundColor = window.Items[index + window.ScrollOffset].GetForegroundColor();
                        WriteText(window.X, window.Y + 1 + index,
                            window.Items[index + window.ScrollOffset].GetString()
                                .Pad(GetWidth(window, window.Items[index + window.ScrollOffset])));
                    }

                    break;

                default:
                    DrawHeader(window);

                    Console.BackgroundColor = window.GetBackgroundColor();
                    for (int index = 0;
                        index < window.Height - 1 && index < window.Items.Count;
                        index++)
                    {
                        if (Console.ForegroundColor != window.Items[index + window.ScrollOffset].GetForegroundColor())
                            Console.ForegroundColor = window.Items[index + window.ScrollOffset].GetForegroundColor();
                        WriteText(window.X, window.Y + 1 + index, window.Items[index + window.ScrollOffset].GetString().Pad(window.Width));
                    }

                    break;
            }

            SetDefaultColors();
        }

        private static void Clear(Window window)
        {
            Console.BackgroundColor = window.GetBackgroundColor();
            for (int i = 0; i < window.Height; i++)
                WriteText(window is DialogWindow ? window.X - 1 : window.X, window.Y + i, "".Pad(window.Width));
        }

        public static void Highlight(Window window)
        {
            // Selected
            WriteText(ColorScheme.ActiveHighlightForeground, ColorScheme.ActiveHighlightBackground, window.X, window.Y + 1 + window.Items.IndexOf(window.Items.Selected) - window.ScrollOffset, window.Items.Selected.GetString().Pad(GetWidth(window, window.Items.Selected)));

            // Ascii art
            //Console.ForegroundColor = ColorScheme.TextBackground;
            //Console.BackgroundColor = ColorScheme.ActiveHighlight;
            //Console.SetCursorPosition(window.X, window.Y + 1);
            //Console.WriteAscii(window.Items.Selected.GetString().Pad(Console.WindowWidth / 10));

            // Last selected
            if (window.Items.LastSelected == window.Items.Selected) return;
            WriteText(window.Items.LastSelected.GetForegroundColor(), window.GetBackgroundColor()
                , window.X,
                window.Y + 1 + window.Items.IndexOf(window.Items.LastSelected) - window.ScrollOffset,
                window.Items.LastSelected.GetString().Pad(GetWidth(window, window.Items.LastSelected)));
        }

        public static void DeactivateHighlight(Window window)
        {
            WriteText(ColorScheme.InactiveHighlightForeground, ColorScheme.InactiveHighlightBackground,
                window.X, window.Y + 1 + window.Items.IndexOf(window.Items.Selected) - window.ScrollOffset,
                window.Items.Selected.GetString().Pad(GetWidth(window, window.Items.Selected)));
        }

        public static void Scroll(Window window, int offset, bool force = false)
        {
            int width = 0;
            if (force)
                width = window.Width;
            else
            foreach (object item in window.Items.GetRange(
                window.ScrollOffset - 1 > 0 ? window.ScrollOffset - 1 : 0,
                window.ScrollOffset + window.Height < window.Items.Count - 1 ? window.Height + 1 : window.Height - 1))
                width = item.GetString().Length > width ? item.GetString().Length : width;
            width = width < window.Width - 2 ? width : window.Width - 2;

            Console.MoveBufferArea(window.X, window.Y, width, window.Height - 1, window.X, window.Y - offset);

            SetDefaultColors();
            WriteText(window.X, offset > 0 ? window.Y - 1 : window.Y + window.Height, "".Pad(window.Width));
            DrawHeader(window);
        }

        private static void DrawHeader(Window window)
        {
            switch (window)
            {
                case ErrorDialog errorDialog:
                    WriteText(ColorScheme.ErrorHeaderForeground, ColorScheme.ErrorHeaderBackground,
                        window.X - 1, window.Y - 1,
                        errorDialog.Message.Pad(window.Width));
                    break;

                case DialogWindow dialogWindow:
                    WriteText(ColorScheme.DialogHeaderForeground, ColorScheme.DialogHeaderBackground,
                        window.X - 1, window.Y - 1,
                        dialogWindow.Message.Pad(window.Width));
                    break;

                case BrowserWindow browserWindow:
                    WriteText(ColorScheme.BrowserHeaderForeground, ColorScheme.BrowserHeaderBackground,
                        window.X, window.Y,
                        browserWindow.Items[0] is ParentShortcut
                            ? browserWindow.CurrentDirectory.FullName.Pad(window.Width)
                            : "My Computer".Pad(window.Width));
                    break;

                case PropertiesWindow propertiesWindow:
                    WriteText(ColorScheme.PropertiesHeaderForeground, ColorScheme.PropertiesHeaderBackground,
                        window.X, window.Y,
                        propertiesWindow.Selected.Name.Pad(window.Width));

                    break;

                case TextWindow textWindow:
                    WriteText(ColorScheme.TextHeaderForeground, ColorScheme.TextHeaderBackground,
                        window.X, window.Y,
                        textWindow.File.Name.Pad(window.Width));
                    break;
            }
        }

        private static void DrawButtons(string[] buttons)
        {
            Console.ForegroundColor = ColorScheme.ButtonForeground;
            Console.BackgroundColor = ColorScheme.ButtonBackground;
            Console.SetCursorPosition(1, Console.WindowHeight - 1);

            int x = 2;
            for (int i = 0; i < buttons.Length - 1; i++)
            {
                WriteText(ColorScheme.ButtonForeground, ColorScheme.ButtonBackground, x, Console.WindowHeight - 1,
                    buttons[i]);
                x += buttons[i].Length + 2;
            }
        }

        private static void WriteText(Color foreground, Color background, int x, int y, string text)
        {
            try

            {
                Console.ForegroundColor = foreground;
                Console.BackgroundColor = background;
                Console.SetCursorPosition(x, y);
                Console.Write(text);
            }
            catch (Exception)
            {
                // TODO
            }
        }

        private static void WriteText(int x, int y, string text)
        {
            try
            {
                Console.SetCursorPosition(x, y);
                Console.Write(text);
            }
            catch (Exception)
            {
                // TODO
            }
        }

        private static int GetWidth(Window window, object item)
        {
            return window is DialogWindow _
                ? item.ToString().Length < window.Width - 2 ? item.ToString().Length + 2 : window.Width - 2
                : window.Width;
        }

        private static void SetDefaultColors()
        {
            Console.ForegroundColor = ColorScheme.DefaultForeground;
            Console.BackgroundColor = ColorScheme.DefaultBackground;
        }
    }
}