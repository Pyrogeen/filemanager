﻿using System;
using System.IO;
using File_Manager__MultiThreaded_.GUI.Windows;
using File_Manager__MultiThreaded_.Utilities;

namespace File_Manager__MultiThreaded_.GUI.Dialogs
{
    public class TextDialog : DialogWindow
    {
        public TextDialog(BrowserWindow window, string message, Operations.Action action)
        {
            SetDialogProperties(((FileSystemInfo) window.Items.Selected).FullName);
            SetProperties(window, message);
            Action = action;
            TextBox = new TextBox(X, Y, Width - 2, Items[0].GetString());
        }

        public TextBox TextBox { get; private set; }

        public void SetDialogProperties(string item)
        {
            Items = new Items<object>
            {
                item,
                "Enter",
                "Cancel"
            };
            Items.Selected = Items[1];
            Items.LastSelected = Items.Selected;
        }

        public override bool HandleKey(ConsoleKeyInfo key, WindowManager browserWindows)
        {
            if (TextBox != null)
            {
                if (!TextBox.HandleKey(key, browserWindows)) return false;
                TextBox = null;
                Renderer.RefreshWindow(this, true);
                return false;
            }

            if (base.HandleKey(key, browserWindows))
                Delete(browserWindows);

            switch (key.Key)
            {
                // Change selected
                case ConsoleKey.UpArrow:
                    Items.LastSelected = Items.Selected;
                    ChangeSelected(-1);
                    Renderer.Highlight(this);
                    break;

                // Change selected
                case ConsoleKey.DownArrow:
                    Items.LastSelected = Items.Selected;
                    ChangeSelected(1);
                    Renderer.Highlight(this);
                    break;

                // Enter
                case ConsoleKey.Enter:
                    if (Items.Selected == Items[0])
                    {
                        TextBox = new TextBox(X, Y, Width - 2, Items[0].GetString());
                        Renderer.RefreshText(TextBox);
                    }
                    else if (Items.Selected == Items[1])
                    {
                        Operations.DoAction(browserWindows.SelectedWindow, Action, Items[0].GetString());
                        Delete(browserWindows);
                    }
                    else
                    {
                        Delete(browserWindows);
                    }

                    break;

                case ConsoleKey.F2:
                    Items.LastSelected = Items.Selected;
                    Items.Selected = Items[0];
                    Renderer.Highlight(this);
                    TextBox = new TextBox(X, Y, Width - 2, Items[0].GetString());
                    Renderer.RefreshText(TextBox);
                    break;

                // Quit
                case ConsoleKey.Escape:
                    Delete(browserWindows);
                    break;
            }

            return false;
        }
    }
}