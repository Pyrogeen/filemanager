﻿using System;
using File_Manager__MultiThreaded_.GUI.Windows;
using File_Manager__MultiThreaded_.Utilities;

namespace File_Manager__MultiThreaded_.GUI.Dialogs
{
    public sealed class ErrorDialog : DialogWindow
    {
        public ErrorDialog(Window window, string message)
        {
            SetDialogProperties();
            Items.Selected = Items[0];
            SetProperties(window, message);
        }

        public ErrorDialog(string message)
        {
            SetDialogProperties();
            Items.Selected = Items[0];
            SetProperties(message);
            SetProperties(Console.WindowWidth / 2 - message.Length / 2, Console.WindowHeight / 2 - 2, message.Length, 4);
        }

        private void SetDialogProperties()
        {
            Items = new Items<object>
            {
                "Enter"
            };
        }

        public override bool HandleKey(ConsoleKeyInfo key, WindowManager browserWindows)
        {
            return key.Key == ConsoleKey.Enter || base.HandleKey(key, browserWindows);
        }
    }
}