﻿using File_Manager__MultiThreaded_.GUI.Windows;

namespace File_Manager__MultiThreaded_.GUI.Dialogs
{
    public abstract class DialogWindow : Window
    {
        public string Message { get; private set; }
        protected Operations.Action Action { get; set; }

        public override void SetProperties(Window window)
        {
            SetProperties(window.X + 4, window.Y + window.Height / 2 - Items.Count - 1, window.Width - 6,
                Items.Count + 2);

            int index = Items.IndexOf(Items.Selected);
            Items.Selected = Items[index <= Items.Count - 1 && index - ScrollOffset < Height ? index : 0];
            Items.LastSelected = Items.Selected;
        }

        protected void SetProperties(Window window, string message)
        {
            SetProperties(window.X + 4, window.Y + window.Height / 2 - Items.Count - 1, window.Width - 6,
                Items.Count + 2);

            Items.LastSelected = Items.Selected;
            Message = message;
        }

        protected void SetProperties(string message)
        {
            Message = message;
        }

        protected void Delete(WindowManager browserWindows)
        {
            browserWindows.SelectedWindow.SubWindow = null;
            Renderer.RefreshWindow(browserWindows.SelectedWindow, true);
        }
    }
}