﻿using System;
using File_Manager__MultiThreaded_.GUI.Windows;
using File_Manager__MultiThreaded_.Utilities;

namespace File_Manager__MultiThreaded_.GUI.Dialogs
{
    public class SimpleDialog : DialogWindow
    {
        public SimpleDialog(Window window, string message, Operations.Action action)
        {
            SetDialogProperties();
            Items.Selected = Items[0];
            SetProperties(window, message);
            Action = action;
        }

        private void SetDialogProperties()
        {
            Items = new Items<object>
            {
                "Enter",
                "Cancel"
            };
        }

        public override bool HandleKey(ConsoleKeyInfo key, WindowManager browserWindows)
        {
            if (base.HandleKey(key, browserWindows))
                Delete(browserWindows);

            switch (key.Key)
            {
                // Change selected
                case ConsoleKey.UpArrow:
                    Items.LastSelected = Items.Selected;
                    if (ChangeSelected(-1))
                        Renderer.Scroll(this, -1);
                    Renderer.Highlight(this);
                    break;

                // Change selected
                case ConsoleKey.DownArrow:
                    Items.LastSelected = Items.Selected;
                    if (ChangeSelected(1))
                        Renderer.Scroll(this, 1);
                    Renderer.Highlight(this);
                    break;

                // Enter
                case ConsoleKey.Enter:
                    if (Items.Selected == Items[0])
                        Operations.DoAction(browserWindows.SelectedWindow, Action);
                    browserWindows.SelectedWindow.SubWindow = null;
                    Renderer.RefreshWindow(browserWindows.SelectedWindow, true);
                    break;
            }

            return false;
        }

        protected override bool ChangeSelected(int offset)
        {
            int offsetIndex = Items.IndexOf(Items.Selected) + offset;

            if (offsetIndex < 0 || offsetIndex > Items.Count - 1) return false;

            Items.Selected =
                Items[offsetIndex];
            return false;
        }
    }
}