﻿using System;
using System.Collections.Generic;
using File_Manager__MultiThreaded_.GUI.TextEditor;
using File_Manager__MultiThreaded_.Utilities;

namespace File_Manager__MultiThreaded_.GUI.Dialogs
{
    public sealed class TextBox : Text
    {
        public TextBox(int x, int y, int width, string text)
        {
            SetProperties(x, y, width);
            Items = new List<char>();
            Items.AddRange(text);
            Items.Add(' ');
            ScrollOffset = Items.Count > Width ? Items.Count - Width + 1 : 0;
            Selected = Items.Count - 1;
        }

        public override bool HandleKey(ConsoleKeyInfo key, WindowManager browserWindows)
        {
            if (base.HandleKey(key, browserWindows)) return true;

            switch (key.Key)
            {
                case ConsoleKey.Enter:
                    ((TextDialog) browserWindows.SelectedWindow.SubWindow)
                        .SetDialogProperties(Items.GetString().TrimEnd());
                    Console.CursorVisible = false;
                    return true;

                // Quit
                case ConsoleKey.Escape:
                    Console.CursorVisible = false;
                    return true;
            }

            return false;
        }
    }
}