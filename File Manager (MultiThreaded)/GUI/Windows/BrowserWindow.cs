﻿using System;
using System.IO;
using System.Linq;
using File_Manager__MultiThreaded_.GUI.Dialogs;
using File_Manager__MultiThreaded_.Utilities;

namespace File_Manager__MultiThreaded_.GUI.Windows
{
    public sealed class BrowserWindow : Window
    {
        public BrowserWindow(int index)
        {
            Index = index;
        }

        public DirectoryInfo CurrentDirectory { get; set; }
        public int Index { get; }

        public Window SubWindow { get; set; }

        public void SetProperties(int width = 0, int height = 0)
        {
            if (width != 0)
                SetProperties(width * Index + 2, 1, width, height);

            int selected = Items?.Selected == null ? 0 : Items.IndexOf(Items.Selected);

            Items = Items ?? new Items<object>();

            Items.RemoveRange(0, Items.Count);
            if (CurrentDirectory == null)
            {
                foreach (DriveInfo drive in DriveInfo.GetDrives())
                    Items.Add(drive.RootDirectory);
            }
            else
            {
                Items.Add(new ParentShortcut(CurrentDirectory.Parent));
                Items.AddRange(CurrentDirectory.GetDirectories().ToList());
                Items.AddRange(CurrentDirectory.GetFiles().ToList());
            }

            ScrollOffset = 0;
            Items.Selected = Items[selected <= Items.Count - 1 && selected - ScrollOffset < Height ? selected : 0];
            Items.LastSelected = Items.Selected;
        }

        public override bool HandleKey(ConsoleKeyInfo key, WindowManager browserWindows)
        {
            if (SubWindow != null)
            {
                if (!SubWindow.HandleKey(key, browserWindows)) return false;
                SubWindow = null;
                Renderer.RefreshWindow(this, true);

                return false;
            }

            if (base.HandleKey(key, browserWindows)) return true;
            switch (key.Key)
            {
                // Open
                case ConsoleKey.Enter:
                    if (Operations.Open(this))
                    {
                        if(WindowManager.MainWindow == null)
                        {
                            SetProperties();
                            Renderer.RefreshWindow(this, true);
                        }
                        else
                            Renderer.DrawAll(browserWindows);
                    }
                    else
                    {
                        Renderer.DeactivateHighlight(this);
                        Renderer.RefreshWindow(SubWindow, true);
                    }

                    break;

                // Change selected
                case ConsoleKey.UpArrow:
                    Items.LastSelected = Items.Selected;
                    if (ChangeSelected(-1))
                        Renderer.Scroll(this, -1);
                    Renderer.Highlight(this);
                    break;

                // Change selected
                case ConsoleKey.DownArrow:
                    Items.LastSelected = Items.Selected;
                    if (ChangeSelected(1))
                        Renderer.Scroll(this, 1);
                    Renderer.Highlight(this);
                    break;

                // Properties
                case ConsoleKey.F1:
                    SubWindow = new PropertiesWindow(browserWindows.SelectedWindow);
                    Renderer.RefreshWindow(SubWindow, true);
                    break;

                // RenMove
                case ConsoleKey.F2:
                    if (Items.Selected is ParentShortcut) break;
                    Renderer.DeactivateHighlight(this);
                    SubWindow = new TextDialog(this, "Rename|Move " + Items.Selected.GetString() + " to:",
                        Operations.Action.RenMove);
                    Renderer.RefreshWindow(SubWindow, true);
                    break;

                // New
                case ConsoleKey.F3:
                    if (Items.Selected is ParentShortcut) break;
                    Renderer.DeactivateHighlight(this);
                    SubWindow = new TextDialog(this, "Create directory/File:", Operations.Action.New);
                    Renderer.RefreshWindow(SubWindow, true);
                    break;

                // Del
                case ConsoleKey.F4:
                    if (Items.Selected is ParentShortcut) break;
                    Renderer.DeactivateHighlight(this);
                    SubWindow = new SimpleDialog(this, "Delete " + Items.Selected.GetString() + "?",
                        Operations.Action.Delete);
                    Renderer.RefreshWindow(SubWindow, true);
                    break;


                // Copy
                case ConsoleKey.F6:
                    SubWindow = new SimpleDialog(this,
                        "Copy " + ((FileSystemInfo) browserWindows.SelectedWindow.Items.Selected).Name +
                        " to clipboard?",
                        Operations.Action.Copy);

                    Renderer.DeactivateHighlight(this);
                    Renderer.RefreshWindow(SubWindow, true);
                    break;

                // CopyTo
                case ConsoleKey.F7:
                    if (Items.Selected is ParentShortcut) break;
                    Renderer.DeactivateHighlight(this);
                    SubWindow = new TextDialog(this, "Copy " + Items.Selected.GetString() + " to:",
                        Operations.Action.CopTo);
                    Renderer.RefreshWindow(SubWindow, true);
                    break;

                // Paste
                case ConsoleKey.F8:
                    if (Operations.Clipboard == null)
                        SubWindow = new ErrorDialog(this, "Nothing to paste");
                    else
                        SubWindow = new SimpleDialog(this, "Paste " + Operations.Clipboard.Name + "?",
                            Operations.Action.Paste);

                    Renderer.DeactivateHighlight(this);
                    Renderer.RefreshWindow(SubWindow, true);
                    break;

                // Del
                case ConsoleKey.Delete:
                    if (Items.Selected is ParentShortcut) break;
                    Renderer.DeactivateHighlight(this);
                    SubWindow = new SimpleDialog(this, "Delete " + Items.Selected.GetString() + "?",
                        Operations.Action.Delete);
                    Renderer.RefreshWindow(SubWindow, true);
                    break;
            }

            return false;
        }
    }
}