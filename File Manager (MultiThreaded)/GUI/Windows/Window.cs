﻿using File_Manager__MultiThreaded_.Utilities;
using System;

namespace File_Manager__MultiThreaded_.GUI.Windows
{
    public abstract class Window
    {
        protected Window(int x = 0, int y = 0, int width = 0, int height = 0)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        public int X { get; private set; }
        public int Y { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }
        public int ScrollOffset { get; set; }
        public Items<object> Items { get; protected set; }

        protected void SetProperties(int x = 0, int y = 0, int width = 0, int height = 0)
        {
            if (x != 0)
                X = x;
            if (y != 0)
                Y = y;
            if (width != 0)
                Width = width;
            if (height != 0)
                Height = height;
        }

        public virtual void SetProperties(Window window)
        {
            X = window.X;
            Y = window.Y;
            Width = window.Width;
            Height = window.Height;
        }

        public virtual bool HandleKey(ConsoleKeyInfo key, WindowManager browserWindows)
        {
            switch (key.Key)
            {
                // Next window
                case ConsoleKey.Tab:
                    Renderer.DeactivateHighlight(browserWindows.SelectedWindow.SubWindow ??
                                                 browserWindows.SelectedWindow);
                    browserWindows.NextWindow();

                    break;

                // Add window
                case ConsoleKey.Add:
                    if (browserWindows.Windows.Count > 9) break;
                    browserWindows.ChangeWindowCount(+1);
                    browserWindows.SetWindows();
                    Renderer.DrawAll(browserWindows);
                    break;

                // Delete window
                case ConsoleKey.Subtract:
                    if (browserWindows.Windows.Count < 2) break;
                    browserWindows.ChangeWindowCount(-1);
                    browserWindows.SetWindows();
                    Renderer.DrawAll(browserWindows);
                    break;

                // RefreshWindow
                case ConsoleKey.F5:
                    browserWindows.SetWindows();
                    Renderer.DrawAll(browserWindows);
                    break;

                // Fullscreen
                case ConsoleKey.F11:
                    break;

                // Quit
                case ConsoleKey.Escape:
                    if (browserWindows.SelectedWindow.SubWindow == null)
                    {
                        Renderer.DeactivateHighlight(this);
                        return true;
                    }
                    else
                    {
                        browserWindows.SelectedWindow.SubWindow = null;
                        Renderer.RefreshWindow(browserWindows.SelectedWindow, true);
                        return true;
                    }
            }

            return false;
        }

        protected virtual bool ChangeSelected(int offset)
        {
            int offsetIndex = Items.IndexOf(Items.Selected) + offset;

            if (offsetIndex < 0 || offsetIndex > Items.Count - 1) return false;

            if (offsetIndex >= ScrollOffset && offset < 0 ||
                offsetIndex - ScrollOffset < Height - 1 && offset > 0)
            {
                Items.Selected =
                    Items[offsetIndex];
                return false;
            }

            ScrollOffset += offset;
            Items.Selected = Items[offsetIndex];
            return true;
        }
    }
}