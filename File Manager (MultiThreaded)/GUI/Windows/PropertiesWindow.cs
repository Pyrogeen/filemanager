﻿using System;
using System.IO;
using File_Manager__MultiThreaded_.Utilities;

namespace File_Manager__MultiThreaded_.GUI.Windows
{
    public sealed class PropertiesWindow : Window
    {
        public PropertiesWindow(BrowserWindow window)
        {
            Selected = (FileSystemInfo) window.Items.Selected;
            SetFileProperties();
            Items.Selected = Items[0];
            SetProperties(window);
        }

        public FileSystemInfo Selected { get; }

        public override void SetProperties(Window window)
        {
            base.SetProperties(window);

            ScrollOffset = 0;
            int index = Items.IndexOf(Items.Selected);
            Items.Selected = Items[index <= Items.Count - 1 && index - ScrollOffset < Height ? index : 0];
            Items.LastSelected = Items.Selected;
        }

        private void SetFileProperties()
        {
            Items = new Items<object>
            {
                "Location: " + Selected.FullName,
                "Created: " + Selected.CreationTime,
                "Modified: " + Selected.LastWriteTime
            };
            switch (Selected)
            {
                case DirectoryInfo directoryInfo:
                    Operations.SetProperties(this, directoryInfo);
                    Items.Add("Directories: " + "counting..");
                    Items.Add("Files: " + "counting..");
                    Items.Add("Size: " + "counting..");
                    Items.Add("Type: Directory");
                    break;

                case FileInfo file:
                    Items.Add("Size: " + file.Length / 1024 + " kB");
                    Items.Add("Type: " + (Selected.Extension == "" ? "File" : Selected.Extension));
                    break;
            }
        }

        public void SetFileProperties(int directories, int files, ulong size)
        {
            int index = Items.IndexOf(Items.Selected);
            Items = new Items<object>
            {
                "Location: " + Selected.FullName,
                "Created: " + Selected.CreationTime,
                "Modified: " + Selected.LastWriteTime
            };
            switch (Selected)
            {
                case DirectoryInfo _:
                    Items.Add("Directories: " + directories);
                    Items.Add("Files: " + files);
                    Items.Add("Size: " + size + " kB");
                    Items.Add("Type: Directory");
                    break;

                case FileInfo file:
                    Items.Add("Size: " + file.Length / 1024 + " kB");
                    Items.Add("Type: " + (Selected.Extension == "" ? "File" : Selected.Extension));
                    break;
            }

            Items.Selected = Items[index];
            Items.LastSelected = Items.Selected;
        }

        public override bool HandleKey(ConsoleKeyInfo key, WindowManager browserWindows)
        {
            if (base.HandleKey(key, browserWindows))
                return true;
            switch (key.Key)
            {
                // Change selected
                case ConsoleKey.UpArrow:
                    Items.LastSelected = Items.Selected;
                    if (ChangeSelected(-1))
                        Renderer.Scroll(this, -1);
                    Renderer.Highlight(this);
                    break;

                // Change selected
                case ConsoleKey.DownArrow:
                    Items.LastSelected = Items.Selected;
                    if (ChangeSelected(1))
                        Renderer.Scroll(this, 1);
                    Renderer.Highlight(this);
                    break;
            }

            return false;
        }
    }
}