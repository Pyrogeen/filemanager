﻿using System;
using System.Collections.Generic;
using File_Manager__MultiThreaded_.GUI.Dialogs;
using File_Manager__MultiThreaded_.GUI.TextEditor;
using File_Manager__MultiThreaded_.GUI.Windows;

namespace File_Manager__MultiThreaded_.GUI
{
    public class WindowManager
    {
        public WindowManager()
        {
            Windows = new List<BrowserWindow>
            {
                new BrowserWindow(0),
                new BrowserWindow(1)
            };
            SetWindows();
            SelectedWindow = Windows[0];
        }

        public List<BrowserWindow> Windows { get; }
        public BrowserWindow SelectedWindow { get; private set; }
        public static Window MainWindow { get; set; }

        private int Width { get; set; }
        private int Height { get; set; }

        private void SetProperties()
        {
            Width = Console.WindowWidth / Windows.Count;
            Height = Console.WindowHeight - 3;
        }

        public void SetWindows()
        {
            if (MainWindow != null)
            {
                if(MainWindow is TextWindow textWindow)
                    textWindow.SetProperties();
                return;
            }
            SetProperties();
            foreach (BrowserWindow window in Windows)
            {
                window.SetProperties(Width, Height);
                if (window.SubWindow != null)
                {
                    window.SubWindow.SetProperties(window);
                    if (window.SubWindow is TextDialog textDialog)
                        textDialog.TextBox?.SetProperties(window.SubWindow);
                }

                window.SubWindow?.SetProperties(window);
            }
        }

        public void ChangeWindowCount(int offset)
        {
            SelectedWindow = Windows.IndexOf(SelectedWindow) > Windows.Count - 1 + offset
                ? Windows[Windows.Count - 1 + offset]
                : SelectedWindow;
            if (offset > 0)
                Windows.Add(new BrowserWindow(Windows.Count));
            else
                Windows.Remove(Windows[Windows.Count - 1]);
        }

        public void NextWindow()
        {
            SelectedWindow = Windows[SelectedWindow.Index + 1 <= Windows.Count - 1 ? SelectedWindow.Index + 1 : 0];
            Renderer.Highlight(SelectedWindow.SubWindow ?? SelectedWindow);
        }
    }
}