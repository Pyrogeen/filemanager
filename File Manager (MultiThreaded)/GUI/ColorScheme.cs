﻿using System.Drawing;

namespace File_Manager__MultiThreaded_.GUI
{
    public static class ColorScheme
    {
        // Defaults
        public static readonly Color DefaultForeground = Color.FromArgb(220, 240, 255);
        public static readonly Color DefaultBackground = Color.FromArgb(20, 10, 70);

        // Browser Window
        public static readonly Color BrowserWindowBackground = Color.FromArgb(20, 90, 190);

        // FileSystem
        public static readonly Color ParentShortcutForeground = Color.FromArgb(220, 240, 255);
        public static readonly Color DirectoryForeground = Color.FromArgb(220, 240, 255);
        public static readonly Color FileForeground = Color.FromArgb(240, 150, 80);

        // Properties Window header
        public static readonly Color PropertiesHeaderForeground = Color.FromArgb(220, 240, 255);
        public static readonly Color PropertiesHeaderBackground = Color.FromArgb(215, 105, 40);

        // Browser Window header
        public static readonly Color BrowserHeaderForeground = Color.FromArgb(220, 240, 255);
        public static readonly Color BrowserHeaderBackground = Color.FromArgb(215, 105, 40);

        // Active Highlight
        public static readonly Color ActiveHighlightForeground = Color.FromArgb(15, 5, 65);
        public static readonly Color ActiveHighlightBackground = Color.FromArgb(250, 200, 100);

        // Inactive Highlight
        public static readonly Color InactiveHighlightForeground = Color.FromArgb(220, 240, 255);
        public static readonly Color InactiveHighlightBackground = Color.FromArgb(150, 150, 180);

        // Dialog Window
        public static readonly Color DialogWindowForeground = Color.FromArgb(220, 240, 255);
        public static readonly Color DialogWindowBackground = Color.FromArgb(50, 120, 255);

        // Dialog Window header
        public static readonly Color DialogHeaderForeground = Color.FromArgb(220, 240, 255);
        public static readonly Color DialogHeaderBackground = Color.FromArgb(215, 105, 40);

        // Button
        public static readonly Color ButtonForeground = Color.FromArgb(15, 5, 65);
        public static readonly Color ButtonBackground = Color.FromArgb(215, 105, 40);

        // Error Window
        public static readonly Color ErrorWindowForeground = Color.FromArgb(220, 240, 255);
        public static readonly Color ErrorWindowBackground = Color.FromArgb(50, 120, 255);

        // Error Window header
        public static readonly Color ErrorHeaderForeground = Color.FromArgb(220, 240, 255);
        public static readonly Color ErrorHeaderBackground = Color.FromArgb(200, 65, 65);

        // Text Window
        public static readonly Color TextWindowForeground = Color.FromArgb(220, 240, 255);
        public static readonly Color TextWindowBackground = Color.FromArgb(15, 5, 65);

        // Text Window header
        public static readonly Color TextHeaderForeground = Color.FromArgb(220, 240, 255);
        public static readonly Color TextHeaderBackground = Color.FromArgb(215, 105, 40);
    }
}