﻿using File_Manager__MultiThreaded_.GUI;
using File_Manager__MultiThreaded_.GUI.TextEditor;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using File_Manager__MultiThreaded_.GUI.Dialogs;
using File_Manager__MultiThreaded_.GUI.Windows;

namespace File_Manager__MultiThreaded_.Utilities
{
    public static class Extensions
    {
        public static Color GetForegroundColor(this object item)
        {
            switch (item)
            {
                case DirectoryInfo _:
                    return ColorScheme.DirectoryForeground;

                case FileInfo _:
                    return ColorScheme.FileForeground;

                case ParentShortcut _:
                    return ColorScheme.ParentShortcutForeground;

                case ErrorDialog _:
                    return ColorScheme.ErrorWindowForeground;

                case DialogWindow _:
                    return ColorScheme.DialogWindowForeground;

                case  TextWindow _:
                    return ColorScheme.TextWindowForeground;

                default: return ColorScheme.DefaultForeground;
            }
        }

        public static Color GetBackgroundColor(this object item)
        {
            switch (item)
            {
                case BrowserWindow _:
                    return ColorScheme.BrowserWindowBackground;

                case PropertiesWindow _:
                    return ColorScheme.BrowserWindowBackground;

                case ErrorDialog _:
                    return ColorScheme.ErrorWindowBackground;

                case DialogWindow _:
                    return ColorScheme.DialogWindowBackground;

                case TextWindow _:
                    return ColorScheme.TextWindowBackground;

                default: return ColorScheme.DefaultBackground;
            }
        }

        public static string GetString(this object item)
        {
            switch (item)
            {
                case FileSystemInfo info:
                    return info.Name;

                case string stringItem:
                    return stringItem;

                case List<char> items:
                    string result = "";
                    foreach (char charItem in items) result += charItem;
                    return result;

                case TextField textField:
                    return textField.Items.GetString();

                default: return null;
            }
        }

        public static string Pad(this string text, int width)
        {
            if (text == null) return "".PadRight(width - 2);

            return text.Length > width - 2 ? text.Substring(0, width - 2) : text.PadRight(width - 2);
        }
    }
}