﻿using System;
using System.IO;
using System.Runtime.Serialization;

namespace File_Manager__MultiThreaded_.Utilities
{
    [Serializable]
    public class ParentShortcut : FileSystemInfo
    {
        public ParentShortcut(FileSystemInfo parent)
        {
            Name = "..";
            if (parent == null)
            {
                Exists = false;
                return;
            }

            FullPath = parent.FullName;
            Exists = parent.Exists;
        }

        protected ParentShortcut(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public override string Name { get; }
        public override bool Exists { get; }

        public override void Delete()
        {
            throw new NotImplementedException();
        }
    }
}