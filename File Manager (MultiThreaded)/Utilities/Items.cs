﻿using System.Collections.Generic;

namespace File_Manager__MultiThreaded_.Utilities
{
    public class Items<T> : List<T>
    {
        public T Selected { get; set; }
        public T LastSelected { get; set; }
    }
}