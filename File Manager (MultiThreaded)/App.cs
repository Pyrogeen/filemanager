﻿using File_Manager__MultiThreaded_.GUI;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using Console = Colorful.Console;

namespace File_Manager__MultiThreaded_
{
    public class App
    {
        private List<Thread> Threads { get; set; }
        private WindowManager BrowserWindows { get; set; }

        private bool Break { get; set; }

        private int ConsoleWidth { get; set; }
        private int ConsoleHeight { get; set; }

        public void Initialize()
        {
            BrowserWindows = new WindowManager();
            SetConsole();
            InitializeThreads();
        }

        private void SetConsole()
        {
            Console.Title = "File Manager";
            Console.CursorVisible = false;
            Console.OutputEncoding = Encoding.Unicode;
            Console.SetWindowSize(Console.LargestWindowWidth / 2, Console.LargestWindowHeight / 2);
        }

        private void InitializeThreads()
        {
            Threads = new List<Thread>
            {
                // Console input
                new Thread(() =>
                {
                    while (!Break)
                        if (WindowManager.MainWindow == null)
                            Break = BrowserWindows.SelectedWindow.HandleKey(Console.ReadKey(true), BrowserWindows);
                        else if (WindowManager.MainWindow.HandleKey(Console.ReadKey(true), BrowserWindows))
                        {
                            WindowManager.MainWindow = null;
                            Renderer.DrawAll(BrowserWindows);
                        }
                }),

                // Window resize
                new Thread(() =>
                {
                    while (true)
                        if (IsResized())
                        {
                            if (Console.WindowHeight < 1) continue;
                            BrowserWindows.SetWindows();
                            Renderer.DrawAll(BrowserWindows);
                        }

                    // ReSharper disable once FunctionNeverReturns
                }) {IsBackground = true}
            };

            Break = false;

            foreach (Thread thread in Threads)
                thread.Start();
        }

        private bool IsResized()
        {
            if (ConsoleWidth == Console.WindowWidth && ConsoleHeight == Console.WindowHeight) return false;
            Console.BackgroundColor = ColorScheme.DefaultBackground;
            try
            {
                Console.Clear();
            }
            catch (IOException)
            {
                /* TODO */
            }

            ConsoleWidth = Console.WindowWidth;
            ConsoleHeight = Console.WindowHeight;
            Thread.Sleep(100);
            IsResized();
            return true;
        }
    }
}