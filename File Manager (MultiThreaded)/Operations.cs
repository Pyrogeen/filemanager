﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using File_Manager__MultiThreaded_.GUI;
using File_Manager__MultiThreaded_.GUI.Dialogs;
using File_Manager__MultiThreaded_.GUI.TextEditor;
using File_Manager__MultiThreaded_.GUI.Windows;
using File_Manager__MultiThreaded_.Utilities;

namespace File_Manager__MultiThreaded_
{
    public static class Operations
    {
        public enum Action
        {
            Open,
            Copy,
            CopTo,
            Paste,
            RenMove,
            New,
            Delete
        }

        public static FileSystemInfo Clipboard { get; private set; }

        public static void DoAction(BrowserWindow window, Action action, string param = "")
        {
            new Thread(() =>
            {
                switch (action)
                {
                    case Action.Open:
                        Open(window);
                        break;
                    case Action.Copy:
                        Copy(window);
                        break;
                    case Action.CopTo:
                        CopyTo(window, param);
                        break;
                    case Action.Paste:
                        Paste(window);
                        break;
                    case Action.RenMove:
                        RenMove(window, param);
                        break;
                    case Action.New:
                        New(window, param);
                        break;
                    case Action.Delete:
                        Delete(window);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(action), action, null);
                }

                window.SetProperties();
                Thread.Sleep(100);
                Renderer.RefreshWindow(window.SubWindow ?? window, true);
            }) {IsBackground = true}.Start();
        }

        public static void SetProperties(PropertiesWindow window, DirectoryInfo directoryInfo)
        {
            new Thread(() =>
            {
                List<DirectoryInfo> directories = GetAllDirectories(directoryInfo);
                List<FileInfo> files = GetAllFiles(directoryInfo);
                window.SetFileProperties(directories.Count, files.Count, GetFilesSize(files));
                Renderer.RefreshWindow(window, true);
            }) {IsBackground = true}.Start();
        }

        public static bool Open(BrowserWindow window)
        {
            switch (window.Items.Selected)
            {
                case ParentShortcut parentShortcut:
                    if (!AccessCheck((FileSystemInfo) window.Items.Selected, window)) return false;

                    window.CurrentDirectory = parentShortcut.Exists ? new DirectoryInfo(parentShortcut.FullName) : null;
                    break;

                case DirectoryInfo directoryInfo:
                    if (!AccessCheck((FileSystemInfo) window.Items.Selected, window)) return false;
                    window.ScrollOffset = 0;
                    window.CurrentDirectory = directoryInfo;
                    window.Items.Selected = window.Items[0];
                    break;

                case FileInfo fileInfo:
                    WindowManager.MainWindow = new TextWindow(fileInfo);
                    break;
            }

            return true;
        }

        public static void GetFile(FileInfo fileInfo, TextWindow window)
        {
            if (!AccessCheck(fileInfo)) return;
            //if (fileInfo.Length > 1000000000) return;
            new Thread(() =>
            {
                using (StreamReader reader = new StreamReader(fileInfo.FullName))
                {
                    while(!reader.EndOfStream)
                    window.Items.Add(new TextField(reader.ReadLine()));
                }
            }) {IsBackground = true}.Start();
            Thread.Sleep(500);
        }

        // Simple
        private static void Copy(BrowserWindow window)
        {
            Clipboard = (FileSystemInfo) window.Items.Selected;
        }

        // Text
        private static void CopyTo(BrowserWindow window, string param)
        {
            switch (window.Items.Selected)
            {
                case DirectoryInfo directoryInfo:
                    List<FileSystemInfo> fileSystems = new List<FileSystemInfo>();
                    fileSystems.AddRange(GetAllDirectories(directoryInfo));
                    fileSystems.AddRange(GetAllFiles(directoryInfo));
                    foreach (FileSystemInfo item in fileSystems)
                        switch (item)
                        {
                            case DirectoryInfo dir:
                                Directory.CreateDirectory(param + Path.DirectorySeparatorChar + directoryInfo.Name +
                                                          Path.DirectorySeparatorChar +
                                                          dir.FullName.Replace(directoryInfo.FullName, ""));
                                break;

                            case FileInfo file:
                                File.Copy(file.FullName,
                                    param + Path.DirectorySeparatorChar +
                                    directoryInfo.Name + Path.DirectorySeparatorChar +
                                    file.FullName.Replace(directoryInfo.FullName, ""), true);
                                break;
                        }
                    break;
                case FileInfo fileInfo:
                    File.Copy(fileInfo.FullName,
                        window.CurrentDirectory.FullName + Path.DirectorySeparatorChar + fileInfo.Name, true);
                    break;
            }
        }

        // Simple
        private static void Paste(BrowserWindow window)
        {
            switch (Clipboard)
            {
                case DirectoryInfo directoryInfo:
                    List<FileSystemInfo> fileSystems = new List<FileSystemInfo>();
                    fileSystems.AddRange(GetAllDirectories(directoryInfo));
                    fileSystems.AddRange(GetAllFiles(directoryInfo));
                    foreach (FileSystemInfo item in fileSystems)
                        switch (item)
                        {
                            case DirectoryInfo dir:
                                Directory.CreateDirectory(window.CurrentDirectory.FullName + Path.DirectorySeparatorChar + directoryInfo.Name + Path.DirectorySeparatorChar + dir.FullName.Replace(Clipboard.FullName, ""));
                                break;

                            case FileInfo file:
                                File.Copy(file.FullName,
                                    window.CurrentDirectory.FullName + Path.DirectorySeparatorChar +
                                    directoryInfo.Name + Path.DirectorySeparatorChar +
                                    file.FullName.Replace(Clipboard.FullName, ""), true);
                                break;
                        }
                    break;
                case FileInfo fileInfo:
                    File.Copy(fileInfo.FullName,
                        window.CurrentDirectory.FullName + Path.DirectorySeparatorChar + fileInfo.Name, true);
                    break;
            }
        }

        // Text
        private static void RenMove(BrowserWindow window, string param)
        {
            switch (window.Items.Selected)
            {
                case FileInfo fileInfo:

                    try
                    {
                        File.Move(fileInfo.FullName, param);
                    }
                    catch (Exception e)
                    {
                        window.SubWindow = new ErrorDialog(window, e.Message);
                    }

                    break;

                case DirectoryInfo directoryInfo:
                    try
                    {
                        Directory.Move(directoryInfo.FullName, param);
                    }
                    catch (Exception e)
                    {
                        window.SubWindow = new ErrorDialog(window, e.Message);
                    }

                    break;
            }
        }

        // Text
        private static void New(BrowserWindow window, string param)
        {
            try
            {
                if (param.Contains('.'))
                    File.Create(param).Dispose();
                else
                    Directory.CreateDirectory(param);
            }
            catch (Exception e)
            {
                window.SubWindow = new ErrorDialog(window, e.Message);
                Renderer.RefreshWindow(window.SubWindow, true);
            }
        }

        // Simple
        private static void Delete(BrowserWindow window)
        {
            switch (window.Items.Selected)
            {
                case DirectoryInfo directoryInfo:
                    Directory.Delete(directoryInfo.FullName, true);
                    break;
                case FileInfo fileInfo:
                    File.Delete(fileInfo.FullName);
                    break;
            }
        }

        private static bool AccessCheck(FileSystemInfo item, BrowserWindow window = null)
        {
            try
            {
                switch (item)
                {
                    case ParentShortcut parentShortcut:
                        if (parentShortcut.Exists)
                            new DirectoryInfo(parentShortcut.FullName).GetFileSystemInfos();
                        break;
                    case DirectoryInfo directoryInfo:
                        directoryInfo.GetFileSystemInfos();
                        break;
                    case FileInfo fileInfo:
                        fileInfo.Open(FileMode.Open).Dispose();
                        break;
                }

                return true;
            }
            catch (Exception e)
            {
                if (window == null)
                    WindowManager.MainWindow = new ErrorDialog(e.Message);
                else
                    window.SubWindow = new ErrorDialog(window, e.Message);
                return false;
            }
        }

        private static List<DirectoryInfo> GetAllDirectories(DirectoryInfo source)
        {
            List<DirectoryInfo> result = new List<DirectoryInfo>();
            try
            {
                result.AddRange(source.GetDirectories());
                foreach (DirectoryInfo subDirectory in source.GetDirectories())
                    result.AddRange(GetAllDirectories(subDirectory));
            }
            catch (Exception)
            {
                //Renderer.WriteError(e.Message);
            }

            return result;
        }

        private static List<FileInfo> GetAllFiles(DirectoryInfo source)
        {
            List<FileInfo> result = new List<FileInfo>();
            try
            {
                result.AddRange(source.GetFiles());
                foreach (DirectoryInfo subDirectory in source.GetDirectories())
                    result.AddRange(GetAllFiles(subDirectory));
            }
            catch (Exception)
            {
                // TODO
            }

            return result;
        }

        private static ulong GetFilesSize(List<FileInfo> files)
        {
            ulong size = 0;
            foreach (FileInfo file in files) size += (ulong) file.Length / 1024;

            return size;
        }
    }
}